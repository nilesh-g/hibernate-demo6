package com.sunbeaminfo.sh.pizza.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.sunbeaminfo.sh.pizza.entities.Item;
import com.sunbeaminfo.sh.pizza.entities.ItemPrice;
import com.sunbeaminfo.sh.pizza.utils.HbUtil;

public class MenuDao {
	public Item getItem(int id) {
		Session session = HbUtil.getSession();
		return session.get(Item.class, id);
	}
	public ItemPrice getItemPrice(int id) {
		Session session = HbUtil.getSession();
		return session.get(ItemPrice.class, id);
	}
	public List<String> getTypes() {
		Session session = HbUtil.getSession();
		String hql = "select distinct i.type from Item i";
		Query q = session.createQuery(hql);
		return q.getResultList();
	}
	public List<String> getCategories(String type) {
		Session session = HbUtil.getSession();
		String hql = "select distinct i.category from Item i where i.type=:p_type";
		Query q = session.createQuery(hql);
		q.setParameter("p_type", type);
		return q.getResultList();
	}
	
	public List<String> getItems(String type, String category) {
		Session session = HbUtil.getSession();
		String hql = "from Item i where i.type=:p_type and i.category=:p_category";
		Query q = session.createQuery(hql);
		q.setParameter("p_type", type);
		q.setParameter("p_category", category);
		return q.getResultList();
	}
}
