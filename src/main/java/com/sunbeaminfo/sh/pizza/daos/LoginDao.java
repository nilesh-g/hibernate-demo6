package com.sunbeaminfo.sh.pizza.daos;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.sunbeaminfo.sh.pizza.entities.Customer;
import com.sunbeaminfo.sh.pizza.utils.HbUtil;

public class LoginDao {
	public Customer getCustomer(String email) {
		Session session = HbUtil.getSession();
		Query<Customer> q = session.createQuery("from Customer c where c.email=:p_email");
		q.setParameter("p_email", email);
		return q.getSingleResult();
	}
	
	public void addCustomer(Customer cust) {
		Session session = HbUtil.getSession();
		session.persist(cust);
	}
	
	public int changePassword(Customer cust, String newPassword) {
		Session session = HbUtil.getSession();
		String hql = "update Customer c set c.password=:p_newPassword where c.id=:p_id";
		Query q = session.createQuery(hql);
		q.setParameter("p_newPassword", newPassword);
		q.setParameter("p_id", cust.getId());
		int cnt = q.executeUpdate();
		session.refresh(cust);
		return cnt;
	}
}
