package com.sunbeaminfo.sh.pizza.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.sunbeaminfo.sh.pizza.entities.Order;
import com.sunbeaminfo.sh.pizza.utils.HbUtil;

public class OrderDao {
	public void addOrder(Order order) {
		Session session = HbUtil.getSession();
		session.persist(order);
	}
	public Order getOrder(int id) {
		Session session = HbUtil.getSession();
		return session.get(Order.class, id);
	}
	public List<Order> getOrderByStatus(String status) {
		Session session = HbUtil.getSession();
		String hql = "from Order o where o.status = :p_status";
		Query<Order> q = session.createQuery(hql);
		q.setParameter("p_status", status);
		return q.getResultList();
	}
}
