package com.sunbeaminfo.sh.pizza.main;

import java.util.Date;
import com.sunbeaminfo.sh.pizza.daos.LoginDao;
import com.sunbeaminfo.sh.pizza.daos.MenuDao;
import com.sunbeaminfo.sh.pizza.daos.OrderDao;
import com.sunbeaminfo.sh.pizza.entities.Customer;
import com.sunbeaminfo.sh.pizza.entities.ItemPrice;
import com.sunbeaminfo.sh.pizza.entities.Order;
import com.sunbeaminfo.sh.pizza.entities.OrderDetail;
import com.sunbeaminfo.sh.pizza.utils.HbUtil;

public class Hb06Main {
	public static void main(String[] args) {
		OrderDao orderDao = new OrderDao();
		MenuDao menuDao = new MenuDao();
		LoginDao loginDao = new LoginDao();
		try {
			HbUtil.beginTransaction();
			Customer cust = loginDao.getCustomer("nilesh@sunbeaminfo.com");
			//System.out.println("Found: " + cust);
			ItemPrice ip1 = menuDao.getItemPrice(11);
			ItemPrice ip2 = menuDao.getItemPrice(15);
			
			Order order = new Order(0, cust, new Date(), "Pending");
			order.getOrderDetails().add(new OrderDetail(0, order, ip1));
			order.getOrderDetails().add(new OrderDetail(0, order, ip2));
			
			orderDao.addOrder(order);
			System.out.println("Order is placed ...");
			
			HbUtil.commitTransaction();
		} catch (Exception e) {
			HbUtil.rollbackTransaction();
			e.printStackTrace();
		}
		
		HbUtil.shutdown();
	}
}
