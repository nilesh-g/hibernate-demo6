package com.sunbeaminfo.sh.pizza.daos.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sunbeaminfo.sh.pizza.daos.LoginDao;
import com.sunbeaminfo.sh.pizza.entities.Customer;
import com.sunbeaminfo.sh.pizza.utils.HbUtil;

public class LoginDaoTest {
	private LoginDao loginDao;

	@Before
	public void setUp() throws Exception {
		loginDao = new LoginDao();
		HbUtil.beginTransaction();
	}

	@After
	public void tearDown() throws Exception {
		HbUtil.commitTransaction();
	}

	@Test
	public void testGetCustomer() {
		Customer cust = loginDao.getCustomer("nilesh@sunbeaminfo.com");
		assertNotNull("Customer Not Found.", cust);
		assertTrue("Fetched wrong customer", cust.getName().equals("nilesh"));
	}

	@Test
	public void testAddCustomer() {
		Customer cust = new Customer(0, "soham", "Katraj, Pune", "7722093091", "soham@sunbeaminfo.com", "soham");
		loginDao.addCustomer(cust);
		assertTrue("Customer add failed.", true); // test case success
	}
	
	@Test
	public void testChangePassword() {
		String newPassword = "nitin123";
		Customer cust = loginDao.getCustomer("nitin@sunbeaminfo.com");
		int cnt = loginDao.changePassword(cust, newPassword);
		assertTrue("No customer updated.", cnt==1);
		assertTrue("Customer password not updated.", cust.getPassword().equals(newPassword));
		// test case successful.
	}
}
